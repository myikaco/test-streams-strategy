package main

import (
	"context"
	"fmt"
	"os"
	"time"

	//"github.com/joho/godotenv"
	"github.com/go-redis/redis/v8"
)

// DB
var ctxStrat = context.Background()
var redisHost = os.Getenv("REDIS_HOST")
var redisPort = os.Getenv("REDIS_PORT")
var rdb *redis.Client

func initRedis() {
	if redisHost == "" {
		redisHost = "127.0.0.1"
		fmt.Println("REDIS_HOST env var nil, using redis dev address -- " + redisHost)
	}
	if redisPort == "" {
		redisPort = "6379"
		fmt.Println("REDIS_PORT env var nil, using redis dev port -- " + redisPort)
	}
	rdb = redis.NewClient(&redis.Options{
		Addr: redisHost + ":" + redisPort,
	})
}

func main() {
	initRedis()
	fmt.Println("test-strategy broadcasting on Redis stream " + redisHost + ":" + redisPort + "...")
	tempStreamName := "userEvents"

	for {
		newID, err := rdb.XAdd(ctxStrat, &redis.XAddArgs{
			Stream: tempStreamName,
			Values: []string{
				"newEvent",
				time.Now().Local().String(),
			},
		}).Result()
		if err != nil {
			fmt.Println("XADD error -- ", err.Error())
		}

		l, xlenErr := rdb.Do(ctxStrat, "XLEN", tempStreamName).Result()
		if xlenErr != nil {
			fmt.Println("XLEN error -- ", xlenErr.Error())
		}

		if newID != "" {
			fmt.Print("Added to " + tempStreamName + ": " + newID + " / len = ")
			fmt.Println(l)
		}

		time.Sleep(1500 * time.Millisecond)

		if l != nil {
			if l.(int64) > 10 {
				fmt.Println("Resetting DB with flushall...")
				rdb.Do(ctxStrat, "flushall").Result()
			}
		}
	}
}
