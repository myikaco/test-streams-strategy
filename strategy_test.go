package main

import (
	"testing"
)

func TestMain(t *testing.T) {
	initRedis()
	if redisHost == "" {
		t.Fatal("redisHost is empty string, expected default dev host")
	}
	if redisPort == "" {
		t.Fatal("redisPort is empty string, expected default dev port")
	}
}
