module bitbucket.org/myika/test-strategy

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-redis/redis/v8 v8.5.0
)
